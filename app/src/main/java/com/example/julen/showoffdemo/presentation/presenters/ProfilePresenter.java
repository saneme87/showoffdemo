package com.example.julen.showoffdemo.presentation.presenters;

import com.example.julen.showoffdemo.domain.interactor.GetRecentsUseCase;
import com.example.julen.showoffdemo.domain.interactor.GetSelfUseCase;
import com.example.julen.showoffdemo.domain.model.Media;
import com.example.julen.showoffdemo.domain.model.User;
import com.example.julen.showoffdemo.presentation.presenters.base.BasePresenter;
import com.example.julen.showoffdemo.presentation.ui.BaseView;
import com.example.julen.showoffdemo.presentation.ui.ProfileView;

import java.util.List;

public class ProfilePresenter extends BasePresenter {

    private GetSelfUseCase getSelfUseCase;
    private GetRecentsUseCase getRecentsUseCase;

    private ProfileView profileView;

    public ProfilePresenter(GetSelfUseCase getSelfUseCase, GetRecentsUseCase getRecentsUseCase) {
        super(getSelfUseCase, getRecentsUseCase);
        this.getSelfUseCase = getSelfUseCase;
        this.getRecentsUseCase = getRecentsUseCase;
    }

    @Override
    public void initWithView(BaseView view) {
        super.initWithView(view);
        profileView = (ProfileView) view;
    }

    @Override
    public void destroy() {
        super.destroy();
        profileView = null;
    }

    public void getSelf() {
        showLoader();
        getSelfUseCase.execute(new SelfSubscriber());
    }

    public void getRecents(String maxId, String minId, int count) {
        showLoader();
        getRecentsUseCase.setParams(maxId, minId, count);
        getRecentsUseCase.execute(new RecentsSubscriber());
    }

    private class SelfSubscriber extends BaseSubscriber<User> {

        @Override
        public void onNext(User user) {
            ProfilePresenter.this.hideLoader();
            ProfilePresenter.this.profileView.onSelfReceived(user);
        }
    }

    private class RecentsSubscriber extends BaseSubscriber<List<Media>> {

        @Override
        public void onNext(List<Media> recents) {
            ProfilePresenter.this.hideLoader();
            ProfilePresenter.this.profileView.onRecentsReceived(recents);
        }
    }
}
