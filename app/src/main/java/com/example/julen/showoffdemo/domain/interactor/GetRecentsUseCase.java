package com.example.julen.showoffdemo.domain.interactor;

import com.example.julen.showoffdemo.domain.executor.PostExecutionThread;
import com.example.julen.showoffdemo.domain.executor.ThreadExecutor;
import com.example.julen.showoffdemo.domain.interactor.base.UseCase;
import com.example.julen.showoffdemo.domain.repository.UserRepository;

import io.reactivex.Observable;

public class GetRecentsUseCase extends UseCase {

    private UserRepository userRepository;
    private String accessToken;

    private String maxId;
    private String minId;
    private int count;

    public GetRecentsUseCase(ThreadExecutor threadExecutor,
                             PostExecutionThread postExecutionThread,
                             UserRepository userRepository,
                             String accessToken) {
        super(threadExecutor, postExecutionThread);
        this.userRepository = userRepository;
        this.accessToken = accessToken;
    }

    public void setParams(String maxId, String minId, int count) {
        this.maxId = maxId;
        this.minId = minId;
        this.count = count;
    }

    @Override
    protected Observable buildUseCaseObservable() {
        return userRepository.getRecents(accessToken, maxId, minId, count);
    }
}
