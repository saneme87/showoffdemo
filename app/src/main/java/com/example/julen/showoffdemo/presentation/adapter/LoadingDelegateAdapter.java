package com.example.julen.showoffdemo.presentation.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.julen.showoffdemo.R;
import com.example.julen.showoffdemo.domain.adapter.ViewType;
import com.example.julen.showoffdemo.domain.adapter.ViewTypeDelegateAdapter;

public class LoadingDelegateAdapter implements ViewTypeDelegateAdapter {

    private LayoutInflater inflater;

    public LoadingDelegateAdapter(Activity activity) {
        inflater = activity.getLayoutInflater();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new LoadingViewHolder(inflater.inflate(R.layout.item_loading, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, ViewType item) {

    }

    private static class LoadingViewHolder extends RecyclerView.ViewHolder {

        LoadingViewHolder(View itemView) {
            super(itemView);
        }
    }
}
