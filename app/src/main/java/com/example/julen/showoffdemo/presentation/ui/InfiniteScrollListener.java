package com.example.julen.showoffdemo.presentation.ui;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class InfiniteScrollListener extends RecyclerView.OnScrollListener {

    private LinearLayoutManager layoutManager;
    private Callback callback;

    private int previousTotal = 0;
    private boolean loading = true;

    public InfiniteScrollListener(LinearLayoutManager layoutManager, Callback callback) {
        this.layoutManager = layoutManager;
        this.callback = callback;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        if (dy > 0) {
            int visibleItemCount = recyclerView.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int firstVisibleItem = layoutManager.findFirstVisibleItemPosition();

            if (loading) {
                if (totalItemCount > previousTotal) {
                    loading = false;
                    previousTotal = totalItemCount;
                }
            }
            int visibleThreshold = 2;
            if (!loading && (totalItemCount - visibleItemCount)
                    <= (firstVisibleItem + visibleThreshold)) {
                // End has been reached
                callback.endReached();
                loading = true;
            }
        }

    }

    public void setLoading(boolean loading) {
        this.loading = loading;
    }

    public interface Callback {
        void endReached();
    }
}
