package com.example.julen.showoffdemo.domain.model;

import com.example.julen.showoffdemo.data.AppConstants;
import com.example.julen.showoffdemo.domain.adapter.ViewType;

public class Media implements ViewType {

    private Comments comments;
    private Likes likes;
    private Images images;
    private String id;
    private User user;

    public Comments getComments() {
        return comments;
    }

    public Likes getLikes() {
        return likes;
    }

    public Images getImages() {
        return images;
    }

    public String getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    @Override
    public int getViewType() {
        return AppConstants.TYPE_MEDIA;
    }
}
