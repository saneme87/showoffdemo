package com.example.julen.showoffdemo.domain.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

public interface ViewTypeDelegateAdapter {

    RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent);

    void onBindViewHolder(RecyclerView.ViewHolder holder, ViewType item);
}
