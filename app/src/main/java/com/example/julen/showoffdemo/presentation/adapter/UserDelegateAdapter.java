package com.example.julen.showoffdemo.presentation.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.julen.showoffdemo.R;
import com.example.julen.showoffdemo.domain.adapter.ViewType;
import com.example.julen.showoffdemo.domain.adapter.ViewTypeDelegateAdapter;
import com.example.julen.showoffdemo.domain.model.User;
import com.squareup.picasso.Picasso;

public class UserDelegateAdapter implements ViewTypeDelegateAdapter {

    private LayoutInflater inflater;

    public UserDelegateAdapter(Activity activity) {
        inflater = activity.getLayoutInflater();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new UserViewHolder(inflater.inflate(R.layout.item_user, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, ViewType item) {
        ((UserViewHolder) holder).bind((User) item);
    }

    private static class UserViewHolder extends RecyclerView.ViewHolder {

        ImageView userPic;
        TextView userCountMedia;
        TextView userCountFollowers;
        TextView userCountFollowing;
        TextView userName;
        TextView userBio;
        TextView userWebsite;


        UserViewHolder(View itemView) {
            super(itemView);
            userPic = itemView.findViewById(R.id.user_pic);
            userCountMedia = itemView.findViewById(R.id.user_count_media);
            userCountFollowers = itemView.findViewById(R.id.user_count_followers);
            userCountFollowing = itemView.findViewById(R.id.user_count_following);
            userName = itemView.findViewById(R.id.user_name);
            userBio = itemView.findViewById(R.id.user_bio);
            userWebsite = itemView.findViewById(R.id.user_website);
        }

        void bind(User user) {
            Picasso.with(itemView.getContext()).load(user.getProfilePicture()).into(userPic);
            userCountMedia.setText(String.valueOf(user.getUserCounts().getMedia()));
            userCountFollowers.setText(String.valueOf(user.getUserCounts().getFollowedBy()));
            userCountFollowing.setText(String.valueOf(user.getUserCounts().getFollows()));
            userName.setText(user.getFullName());
            userBio.setText(user.getBio());
            userWebsite.setText(user.getWebsite());
        }
    }

}
