package com.example.julen.showoffdemo.data.net.model;

import com.example.julen.showoffdemo.domain.model.User;

public class SelfResponse {

    private User data;

    public User getUser() {
        return data;
    }
}
