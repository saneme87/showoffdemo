package com.example.julen.showoffdemo.data.repository;

import com.example.julen.showoffdemo.data.net.RestAPI;
import com.example.julen.showoffdemo.data.net.model.RecentsResponse;
import com.example.julen.showoffdemo.data.net.model.SelfResponse;
import com.example.julen.showoffdemo.domain.model.Media;
import com.example.julen.showoffdemo.domain.model.User;
import com.example.julen.showoffdemo.domain.repository.UserRepository;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.annotations.NonNull;
import retrofit2.Call;
import retrofit2.Response;

public class UserRepositoryImpl implements UserRepository {

    private RestAPI api;

    public UserRepositoryImpl(RestAPI api) {
        this.api = api;
    }

    @Override
    public Observable<User> getSelf(final String accessToken) {

        return Observable.create(new ObservableOnSubscribe<User>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<User> e) throws Exception {
                Call<SelfResponse> callResponse = api.getSelf(accessToken);
                Response<SelfResponse> response = callResponse.execute();

                if (response.isSuccessful()) {
                    User user = response.body().getUser();
                    e.onNext(user);
                    e.onComplete();
                } else {
                    e.onError(new Throwable(response.message()));
                }
            }
        });
    }

    @Override
    public Observable<List<Media>> getRecents(final String accessToken, final String maxId, final String minId, final int count) {

        return Observable.create(new ObservableOnSubscribe<List<Media>>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<List<Media>> e) throws Exception {
                Call<RecentsResponse> callResponse = api.getRecents(accessToken, maxId, minId, count);
                Response<RecentsResponse> response = callResponse.execute();

                if (response.isSuccessful()) {
                    List<Media> recents = response.body().getData();
                    e.onNext(recents);
                    e.onComplete();
                } else {
                    e.onError(new Throwable(response.message()));
                }
            }
        });
    }
}
