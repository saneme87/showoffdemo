package com.example.julen.showoffdemo.presentation.ui.activities;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;

import com.example.julen.showoffdemo.data.AppConstants;

public class BaseActivity extends AppCompatActivity {

    protected void saveStringToSharedPreferences(String key, String value) {
        SharedPreferences sharedPref = getSharedPreferences(AppConstants.SHARED_PREFERENCES, MODE_PRIVATE);
        sharedPref.edit().putString(key, value).apply();
    }

    protected String getStringFromSharedPreferences(String key, String defaultValue) {
        SharedPreferences sharedPref = getSharedPreferences(AppConstants.SHARED_PREFERENCES, MODE_PRIVATE);
        return sharedPref.getString(key, defaultValue);
    }

    protected void removeFromSharedPreferences(String key) {
        SharedPreferences preferences = getSharedPreferences(AppConstants.SHARED_PREFERENCES, MODE_PRIVATE);
        preferences.edit().remove(key).apply();
    }
}
