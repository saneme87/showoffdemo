package com.example.julen.showoffdemo.domain.repository;

import com.example.julen.showoffdemo.domain.model.Media;
import com.example.julen.showoffdemo.domain.model.User;

import java.util.List;

import io.reactivex.Observable;

public interface UserRepository {
    Observable<User> getSelf(String accessToken);

    Observable<List<Media>> getRecents(String accessToken, String maxId, String minId, int count);
}
