package com.example.julen.showoffdemo.presentation.ui.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;

import com.example.julen.showoffdemo.R;
import com.example.julen.showoffdemo.data.AppConstants;
import com.example.julen.showoffdemo.data.net.RestAPI;
import com.example.julen.showoffdemo.data.net.model.AuthenticationInfo;
import com.example.julen.showoffdemo.data.repository.AuthRepositoryImpl;
import com.example.julen.showoffdemo.domain.executor.impl.JobExecutor;
import com.example.julen.showoffdemo.domain.executor.impl.UIThread;
import com.example.julen.showoffdemo.domain.interactor.GetAccessTokenUseCase;
import com.example.julen.showoffdemo.domain.repository.AuthRepository;
import com.example.julen.showoffdemo.presentation.presenters.LoginPresenter;
import com.example.julen.showoffdemo.presentation.ui.LoginView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class LoginActivity extends BaseActivity implements LoginView {

    @BindView(R.id.progress)
    View progressView;
    @BindView(R.id.login_btn)
    ImageButton loginButton;

    AlertDialog loginDialog;

    private LoginPresenter presenter;
    private AuthRepository repository;
    private AuthenticationInfo authInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        if (loggedIn()) {
            goToProfile();
        } else {
            repository = new AuthRepositoryImpl(RestAPI.getInstance());
            authInfo = new AuthenticationInfo(
                    AppConstants.INSTAGRAM_CLIENT_ID,
                    AppConstants.INSTAGRAM_CLIENT_SECRET,
                    AppConstants.INSTAGRAM_GRANT_TYPE,
                    AppConstants.INSTAGRAM_REDIRECT_URI);

            init();
        }
    }

    private boolean loggedIn() {
        return !getStringFromSharedPreferences(AppConstants.PREFERENCES_ACCESS_TOKEN, "").equals("");
    }

    private void init() {
        GetAccessTokenUseCase useCase = new GetAccessTokenUseCase(
                JobExecutor.getInstance(),
                UIThread.getInstance(),
                repository);
        presenter = new LoginPresenter(useCase);
        presenter.initWithView(this);
    }

    @OnClick(R.id.login_btn)
    public void onLoginButtonClicked() {
        showLoader();
        showLoginDialog();
    }

    private void showLoginDialog() {
        String authUrl = getResources().getString(R.string.instagram_authorization_url,
                AppConstants.INSTAGRAM_CLIENT_ID, AppConstants.INSTAGRAM_REDIRECT_URI);

        AlertDialog.Builder loginBuilder = new AlertDialog.Builder(this);
        loginDialog = loginBuilder.create();

        WebView wv = new WebView(this) {
            @Override
            public boolean onCheckIsTextEditor() {
                return true;
            }
        };
        wv.loadUrl(authUrl);
        wv.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.startsWith(AppConstants.INSTAGRAM_REDIRECT_URI)) {
                    handleRedirectUri(Uri.parse(url));
                    CookieManager.getInstance().removeAllCookie();
                } else {
                    view.loadUrl(url);
                }
                return true;
            }
        });
        loginDialog.setView(wv);
        loginDialog.show();
    }

    private void handleRedirectUri(Uri uri) {
        String code = uri.getQueryParameter("code");
        String error = uri.getQueryParameter("error");
        if (code != null) {
            authInfo.setCode(code);
            presenter.login(authInfo);
        } else if (error != null) {
            handleError(new Throwable(error));
        }
        loginDialog.dismiss();
        hideLoader();
    }

    @Override
    public void onLoginSucceeded(String accessToken) {
        saveStringToSharedPreferences(AppConstants.PREFERENCES_ACCESS_TOKEN, accessToken);
        goToProfile();
    }

    public void goToProfile() {
        Intent intent = new Intent(this, ProfileActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void showLoader() {
        loginButton.setVisibility(GONE);
        progressView.setVisibility(VISIBLE);
    }

    @Override
    public void hideLoader() {
        progressView.setVisibility(GONE);
        loginButton.setVisibility(VISIBLE);
    }

    @Override
    public void handleError(Throwable error) {
        Log.e(AppConstants.APP_TAG, error.getMessage());
    }

}
