package com.example.julen.showoffdemo.presentation.presenters;

import com.example.julen.showoffdemo.data.net.model.AuthenticationInfo;
import com.example.julen.showoffdemo.domain.interactor.GetAccessTokenUseCase;
import com.example.julen.showoffdemo.presentation.presenters.base.BasePresenter;
import com.example.julen.showoffdemo.presentation.ui.BaseView;
import com.example.julen.showoffdemo.presentation.ui.LoginView;

public class LoginPresenter extends BasePresenter {

    private GetAccessTokenUseCase getAccessTokenUseCase;
    private LoginView loginView;

    public LoginPresenter(GetAccessTokenUseCase getAccessTokenUseCase) {
        super(getAccessTokenUseCase);
        this.getAccessTokenUseCase = getAccessTokenUseCase;
    }

    @Override
    public void initWithView(BaseView view) {
        super.initWithView(view);
        loginView = (LoginView) view;
    }

    @Override
    public void destroy() {
        super.destroy();
        loginView = null;
    }

    public void login(AuthenticationInfo authInfo) {
        showLoader();
        getAccessTokenUseCase.setParams(authInfo);
        getAccessTokenUseCase.execute(new LoginSubscriber());
    }

    private class LoginSubscriber extends BaseSubscriber<String> {

        @Override
        public void onNext(String accessToken) {
            LoginPresenter.this.hideLoader();
            LoginPresenter.this.loginView.onLoginSucceeded(accessToken);
        }
    }
}
