package com.example.julen.showoffdemo.presentation.presenters.base;

import com.example.julen.showoffdemo.presentation.ui.BaseView;

public interface Presenter {

    void initWithView(BaseView view);

    void destroy();
}
