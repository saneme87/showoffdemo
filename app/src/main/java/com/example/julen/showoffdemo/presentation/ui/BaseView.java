package com.example.julen.showoffdemo.presentation.ui;

public interface BaseView {

    void showLoader();

    void hideLoader();

    void handleError(Throwable error);
}
