package com.example.julen.showoffdemo.data.net;

import com.example.julen.showoffdemo.data.net.model.AuthenticationResponse;
import com.example.julen.showoffdemo.data.net.model.RecentsResponse;
import com.example.julen.showoffdemo.data.net.model.SelfResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface InstagramAPI {

    String INSTAGRAM_BASE_URL = "https://api.instagram.com";

    @FormUrlEncoded
    @POST("/oauth/access_token")
    Call<AuthenticationResponse> getAuthentication(
            @Field("client_id") String clientId,
            @Field("client_secret") String clientSecret,
            @Field("grant_type") String grantType,
            @Field("redirect_uri") String redirectUri,
            @Field("code") String code);

    @GET("/v1/users/self")
    Call<SelfResponse> getSelf(
            @Query("access_token") String accessToken);

    @GET("/v1/users/self/media/recent")
    Call<RecentsResponse> getRecents(
            @Query("access_token") String accessToken,
            @Query("max_id") String maxId,
            @Query("min_id") String minId,
            @Query("count") int count);
}
