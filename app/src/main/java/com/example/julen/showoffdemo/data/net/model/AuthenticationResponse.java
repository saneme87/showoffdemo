package com.example.julen.showoffdemo.data.net.model;

public class AuthenticationResponse {

    private String access_token;

    public String getAccessToken() {
        return access_token;
    }

}
