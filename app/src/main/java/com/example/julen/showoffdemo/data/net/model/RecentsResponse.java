package com.example.julen.showoffdemo.data.net.model;

import com.example.julen.showoffdemo.domain.model.Media;

import java.util.List;

public class RecentsResponse {
    private List<Media> data;

    public List<Media> getData() {
        return data;
    }
}
