package com.example.julen.showoffdemo.presentation.ui;

import com.example.julen.showoffdemo.domain.model.Media;
import com.example.julen.showoffdemo.domain.model.User;

import java.util.List;

public interface ProfileView extends BaseView {

    void onSelfReceived(User user);

    void onRecentsReceived(List<Media> recents);

}
