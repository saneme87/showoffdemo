package com.example.julen.showoffdemo.data.net.model;

public class AuthenticationInfo {
    private String clientId;
    private String clientSecret;
    private String grantType;
    private String redirectUri;
    private String code;

    public AuthenticationInfo(String clientId, String clientSecret, String grantType, String redirectUri) {
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.grantType = grantType;
        this.redirectUri = redirectUri;
    }

    public String getClientId() {
        return clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public String getGrantType() {
        return grantType;
    }

    public String getRedirectUri() {
        return redirectUri;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
