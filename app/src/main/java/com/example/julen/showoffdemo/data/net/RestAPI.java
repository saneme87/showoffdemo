package com.example.julen.showoffdemo.data.net;

import com.example.julen.showoffdemo.data.net.model.AuthenticationInfo;
import com.example.julen.showoffdemo.data.net.model.AuthenticationResponse;
import com.example.julen.showoffdemo.data.net.model.RecentsResponse;
import com.example.julen.showoffdemo.data.net.model.SelfResponse;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

public class RestAPI {

    private static RestAPI instance = null;
    private InstagramAPI api;

    private RestAPI() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(InstagramAPI.INSTAGRAM_BASE_URL)
                .addConverterFactory(MoshiConverterFactory.create())
                .build();
        api = retrofit.create(InstagramAPI.class);
    }

    public static RestAPI getInstance() {
        if (instance == null) {
            instance = new RestAPI();
        }
        return instance;
    }

    public Call<AuthenticationResponse> getAuthentication(AuthenticationInfo authInfo) {
        return api.getAuthentication(
                authInfo.getClientId(),
                authInfo.getClientSecret(),
                authInfo.getGrantType(),
                authInfo.getRedirectUri(),
                authInfo.getCode());
    }

    public Call<SelfResponse> getSelf(String accessToken) {
        return api.getSelf(accessToken);
    }

    public Call<RecentsResponse> getRecents(String accessToken, String maxId, String minId, int count) {
        return api.getRecents(accessToken, maxId, minId, count);
    }
}
