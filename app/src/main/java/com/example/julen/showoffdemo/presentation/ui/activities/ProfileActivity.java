package com.example.julen.showoffdemo.presentation.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.example.julen.showoffdemo.R;
import com.example.julen.showoffdemo.data.AppConstants;
import com.example.julen.showoffdemo.data.net.RestAPI;
import com.example.julen.showoffdemo.data.repository.UserRepositoryImpl;
import com.example.julen.showoffdemo.domain.executor.impl.JobExecutor;
import com.example.julen.showoffdemo.domain.executor.impl.UIThread;
import com.example.julen.showoffdemo.domain.interactor.GetRecentsUseCase;
import com.example.julen.showoffdemo.domain.interactor.GetSelfUseCase;
import com.example.julen.showoffdemo.domain.model.Media;
import com.example.julen.showoffdemo.domain.model.User;
import com.example.julen.showoffdemo.domain.repository.UserRepository;
import com.example.julen.showoffdemo.presentation.adapter.ProfileAdapter;
import com.example.julen.showoffdemo.presentation.presenters.ProfilePresenter;
import com.example.julen.showoffdemo.presentation.ui.InfiniteScrollListener;
import com.example.julen.showoffdemo.presentation.ui.ProfileView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileActivity extends BaseActivity implements ProfileView, InfiniteScrollListener.Callback {

    private static int POSTS_REQUESTED = 10;

    @BindView(R.id.list_view)
    RecyclerView listView;

    InfiniteScrollListener scrollListener;

    private ProfilePresenter presenter;
    UserRepository repository;

    String lastId;
    boolean noMorePosts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);

        repository = new UserRepositoryImpl(RestAPI.getInstance());

        listView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        listView.setLayoutManager(layoutManager);
        listView.clearOnScrollListeners();

        scrollListener = new InfiniteScrollListener(layoutManager, this);
        listView.addOnScrollListener(scrollListener);

        if (listView.getAdapter() == null) {
            listView.setAdapter(new ProfileAdapter(this));
        }

        noMorePosts = false;

        init();
    }

    private void init() {
        String accessToken = getStringFromSharedPreferences(AppConstants.PREFERENCES_ACCESS_TOKEN, "");
        GetSelfUseCase useCase1 = new GetSelfUseCase(
                JobExecutor.getInstance(),
                UIThread.getInstance(),
                repository,
                accessToken);
        GetRecentsUseCase useCase2 = new GetRecentsUseCase(
                JobExecutor.getInstance(),
                UIThread.getInstance(),
                repository,
                accessToken);
        presenter = new ProfilePresenter(useCase1, useCase2);
        presenter.initWithView(this);

        presenter.getSelf();
    }

    @Override
    public void showLoader() {
    }

    @Override
    public void hideLoader() {
    }

    @Override
    public void handleError(Throwable error) {
        Log.e(AppConstants.APP_TAG, error.getMessage());
        scrollListener.setLoading(false);
    }

    @Override
    public void onSelfReceived(User user) {
        setTitle(user.getUsername());
        ((ProfileAdapter) listView.getAdapter()).addUser(user);
        presenter.getRecents(lastId, "", POSTS_REQUESTED);
    }

    @Override
    public void onRecentsReceived(List<Media> recents) {
        if (recents.size() > 0) {
            lastId = recents.get(recents.size() - 1).getId();
            ((ProfileAdapter) listView.getAdapter()).addRecents(recents);
        } else {
            ((ProfileAdapter) listView.getAdapter()).removeLoading();
            noMorePosts = true;
        }
    }

    @Override
    public void endReached() {
        if (!noMorePosts) {
            presenter.getRecents(lastId, "", POSTS_REQUESTED);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.profile_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_logout:
                logout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void logout() {
        removeFromSharedPreferences(AppConstants.PREFERENCES_ACCESS_TOKEN);
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
