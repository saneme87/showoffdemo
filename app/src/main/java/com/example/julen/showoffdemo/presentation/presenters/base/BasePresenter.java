package com.example.julen.showoffdemo.presentation.presenters.base;

import com.example.julen.showoffdemo.domain.interactor.base.UseCase;
import com.example.julen.showoffdemo.presentation.ui.BaseView;

import io.reactivex.observers.DisposableObserver;

public class BasePresenter implements Presenter {
    private BaseView view;
    private UseCase useCase1, useCase2;

    public BasePresenter(UseCase useCase1) {
        this.useCase1 = useCase1;
    }

    public BasePresenter(UseCase useCase1, UseCase useCase2) {
        this.useCase1 = useCase1;
        this.useCase2 = useCase2;
    }

    @Override
    public void initWithView(BaseView view) {
        this.view = view;
    }

    @Override
    public void destroy() {
        if (this.useCase1 != null) this.useCase1.unsubscribe();
        if (this.useCase2 != null) this.useCase2.unsubscribe();
        this.view = null;
    }

    protected void showLoader() {
        this.view.showLoader();
    }

    protected void hideLoader() {
        this.view.hideLoader();
    }

    protected void handleError(Throwable error) {
        this.view.handleError(error);
    }

    protected class BaseSubscriber<T> extends DisposableObserver<T> {

        @Override
        public void onComplete() {
            BasePresenter.this.hideLoader();
        }

        @Override
        public void onError(Throwable e) {
            BasePresenter.this.hideLoader();
            BasePresenter.this.handleError(e);
            e.printStackTrace();
        }

        @Override
        public void onNext(T t) {
            BasePresenter.this.hideLoader();
        }
    }
}
