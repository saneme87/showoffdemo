package com.example.julen.showoffdemo.domain.interactor;

import com.example.julen.showoffdemo.domain.executor.PostExecutionThread;
import com.example.julen.showoffdemo.domain.executor.ThreadExecutor;
import com.example.julen.showoffdemo.domain.interactor.base.UseCase;
import com.example.julen.showoffdemo.domain.repository.UserRepository;

import io.reactivex.Observable;

public class GetSelfUseCase extends UseCase {

    private UserRepository userRepository;
    private String accessToken;

    public GetSelfUseCase(ThreadExecutor threadExecutor,
                          PostExecutionThread postExecutionThread,
                          UserRepository userRepository,
                          String accessToken) {
        super(threadExecutor, postExecutionThread);
        this.userRepository = userRepository;
        this.accessToken = accessToken;
    }

    @Override
    protected Observable buildUseCaseObservable() {
        return userRepository.getSelf(accessToken);
    }
}
