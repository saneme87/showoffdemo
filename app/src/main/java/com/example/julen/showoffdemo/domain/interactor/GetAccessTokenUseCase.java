package com.example.julen.showoffdemo.domain.interactor;

import com.example.julen.showoffdemo.data.net.model.AuthenticationInfo;
import com.example.julen.showoffdemo.domain.executor.PostExecutionThread;
import com.example.julen.showoffdemo.domain.executor.ThreadExecutor;
import com.example.julen.showoffdemo.domain.interactor.base.UseCase;
import com.example.julen.showoffdemo.domain.repository.AuthRepository;

import io.reactivex.Observable;

public class GetAccessTokenUseCase extends UseCase<String> {

    private AuthRepository authRepository;

    private AuthenticationInfo authInfo;

    public GetAccessTokenUseCase(ThreadExecutor threadExecutor,
                                 PostExecutionThread postExecutionThread,
                                 AuthRepository authRepository) {
        super(threadExecutor, postExecutionThread);
        this.authRepository = authRepository;
    }

    public void setParams(AuthenticationInfo authInfo) {
        this.authInfo = authInfo;
    }

    @Override
    protected Observable<String> buildUseCaseObservable() {
        return authRepository.getAccessToken(authInfo);
    }
}
