package com.example.julen.showoffdemo.domain.repository;

import com.example.julen.showoffdemo.data.net.model.AuthenticationInfo;

import io.reactivex.Observable;

public interface AuthRepository {

    Observable<String> getAccessToken(AuthenticationInfo authInfo);
}
