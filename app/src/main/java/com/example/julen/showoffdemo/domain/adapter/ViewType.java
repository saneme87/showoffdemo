package com.example.julen.showoffdemo.domain.adapter;

/**
 * Created by Julen on 24/07/2017.
 */

public interface ViewType {
    int getViewType();
}
