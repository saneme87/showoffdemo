package com.example.julen.showoffdemo.presentation.ui;

public interface LoginView extends BaseView {
    void onLoginSucceeded(String accessToken);
}
