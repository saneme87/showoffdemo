package com.example.julen.showoffdemo.domain.model;

import com.example.julen.showoffdemo.data.AppConstants;
import com.example.julen.showoffdemo.domain.adapter.ViewType;

public class User implements ViewType {
    private String username;
    private String full_name;
    private String profile_picture;
    private String bio;
    private String website;
    private UserCounts counts;

    public String getUsername() {
        return username;
    }

    public String getFullName() {
        return full_name;
    }

    public String getProfilePicture() {
        return profile_picture;
    }

    public String getBio() {
        return bio;
    }

    public String getWebsite() {
        return website;
    }

    public UserCounts getUserCounts() {
        return counts;
    }

    @Override
    public int getViewType() {
        return AppConstants.TYPE_USER;
    }

}
