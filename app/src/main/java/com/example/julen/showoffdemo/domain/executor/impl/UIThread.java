package com.example.julen.showoffdemo.domain.executor.impl;

import com.example.julen.showoffdemo.domain.executor.PostExecutionThread;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class UIThread implements PostExecutionThread {

    private static UIThread instance = null;

    private UIThread() {
    }

    public static UIThread getInstance() {
        if (instance == null) {
            instance = new UIThread();
        }
        return instance;
    }

    @Override
    public Scheduler getScheduler() {
        return AndroidSchedulers.mainThread();
    }

}