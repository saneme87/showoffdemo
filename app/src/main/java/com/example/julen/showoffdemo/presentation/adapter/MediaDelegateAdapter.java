package com.example.julen.showoffdemo.presentation.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.julen.showoffdemo.R;
import com.example.julen.showoffdemo.domain.adapter.ViewType;
import com.example.julen.showoffdemo.domain.adapter.ViewTypeDelegateAdapter;
import com.example.julen.showoffdemo.domain.model.Media;
import com.example.julen.showoffdemo.presentation.ui.design.SquareImageView;
import com.squareup.picasso.Picasso;

public class MediaDelegateAdapter implements ViewTypeDelegateAdapter {

    private LayoutInflater inflater;

    public MediaDelegateAdapter(Activity activity) {
        inflater = activity.getLayoutInflater();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new MediaViewHolder(inflater.inflate(R.layout.item_media, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, ViewType item) {
        ((MediaViewHolder) holder).bind((Media) item);
    }

    private static class MediaViewHolder extends RecyclerView.ViewHolder {

        ImageView userPic;
        TextView userNick;
        SquareImageView mediaImage;
        TextView mediaLikesCount;
        TextView mediaCommentsCount;

        MediaViewHolder(View itemView) {
            super(itemView);
            userPic = itemView.findViewById(R.id.media_user_pic);
            userNick = itemView.findViewById(R.id.media_user_nick);
            mediaImage = itemView.findViewById(R.id.media_image);
            mediaLikesCount = itemView.findViewById(R.id.media_likes_count);
            mediaCommentsCount = itemView.findViewById(R.id.media_comments_count);
        }

        void bind(Media media) {
            Picasso.with(userPic.getContext()).load(media.getUser().getProfilePicture()).into(userPic);
            userNick.setText(String.valueOf(media.getUser().getUsername()));
            Picasso.with(mediaImage.getContext()).load(media.getImages().getStandardResolution().getUrl()).into(mediaImage);
            mediaLikesCount.setText(String.valueOf(media.getLikes().getCount()));
            mediaCommentsCount.setText(String.valueOf(media.getComments().getCount()));
        }
    }
}
