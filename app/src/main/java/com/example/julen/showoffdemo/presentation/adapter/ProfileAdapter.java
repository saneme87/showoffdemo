package com.example.julen.showoffdemo.presentation.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.example.julen.showoffdemo.domain.adapter.ViewType;
import com.example.julen.showoffdemo.domain.adapter.ViewTypeDelegateAdapter;
import com.example.julen.showoffdemo.domain.model.Media;
import com.example.julen.showoffdemo.domain.model.User;

import java.util.ArrayList;
import java.util.List;

import static com.example.julen.showoffdemo.data.AppConstants.TYPE_LOADING;
import static com.example.julen.showoffdemo.data.AppConstants.TYPE_MEDIA;
import static com.example.julen.showoffdemo.data.AppConstants.TYPE_USER;

public class ProfileAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private SparseArray<ViewTypeDelegateAdapter> delegateAdapters = new SparseArray<ViewTypeDelegateAdapter>();
    private ViewType loadingItem;
    private List<ViewType> items;

    public ProfileAdapter(Activity activity) {
        delegateAdapters.put(TYPE_LOADING, new LoadingDelegateAdapter(activity));
        delegateAdapters.put(TYPE_USER, new UserDelegateAdapter(activity));
        delegateAdapters.put(TYPE_MEDIA, new MediaDelegateAdapter(activity));
        items = new ArrayList<>();
        loadingItem = new ViewType() {
            @Override
            public int getViewType() {
                return TYPE_LOADING;
            }
        };
        items.add(loadingItem);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return delegateAdapters.get(viewType).onCreateViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        delegateAdapters.get(getItemViewType(position)).onBindViewHolder(holder, items.get(position));
    }

    @Override
    public int getItemViewType(int position) {
        return this.items.get(position).getViewType();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void addUser(User user) {
        int initPosition = getItemCount() - 1;
        items.remove(initPosition);
        notifyItemRemoved(initPosition);

        items.add(user);
        items.add(loadingItem);
        notifyItemRangeChanged(initPosition, getItemCount() + 1);
    }

    public void addRecents(List<Media> recents) {
        int initPosition = getItemCount() - 1;
        items.remove(initPosition);
        notifyItemRemoved(initPosition);

        items.addAll(recents);
        items.add(loadingItem);
        notifyItemRangeChanged(initPosition, getItemCount() + 1);
    }

    public void removeLoading() {
        int initPosition = getItemCount() - 1;
        items.remove(initPosition);
        notifyItemRemoved(initPosition);
    }
}
