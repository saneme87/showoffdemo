package com.example.julen.showoffdemo.domain.model;

public class UserCounts {

    private int media;
    private int follows;
    private int followed_by;

    public int getMedia() {
        return media;
    }

    public int getFollows() {
        return follows;
    }

    public int getFollowedBy() {
        return followed_by;
    }
}
