package com.example.julen.showoffdemo.domain.executor;

import java.util.concurrent.Executor;

public interface ThreadExecutor extends Executor {
}
