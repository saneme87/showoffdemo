package com.example.julen.showoffdemo.data;

public class AppConstants {

    //TAG
    public static String APP_TAG = "showoffdemo";

    // API
    public static String INSTAGRAM_CLIENT_ID = "7668e0c9c1b7462591f625d16c029270";
    public static String INSTAGRAM_CLIENT_SECRET = "fcb1baed6fd74e959e92405529643f75";
    public static String INSTAGRAM_GRANT_TYPE = "authorization_code";
    public static String INSTAGRAM_REDIRECT_URI = "https://com.example.julen.showoffdemo.login.callback";

    // ADAPTER
    public static int TYPE_USER = 0;
    public static int TYPE_MEDIA = 1;
    public static int TYPE_LOADING = 2;

    // SHARED PREFERENCES
    public static String SHARED_PREFERENCES = "com.example.julen.shared_preferences";
    public static String PREFERENCES_ACCESS_TOKEN = "pref.access_token";
}
