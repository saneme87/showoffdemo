package com.example.julen.showoffdemo.data.repository;

import com.example.julen.showoffdemo.data.net.RestAPI;
import com.example.julen.showoffdemo.data.net.model.AuthenticationInfo;
import com.example.julen.showoffdemo.data.net.model.AuthenticationResponse;
import com.example.julen.showoffdemo.domain.repository.AuthRepository;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.annotations.NonNull;
import retrofit2.Call;
import retrofit2.Response;

public class AuthRepositoryImpl implements AuthRepository {

    private RestAPI api;

    public AuthRepositoryImpl(RestAPI api) {
        this.api = api;
    }

    @Override
    public Observable<String> getAccessToken(final AuthenticationInfo authInfo) {

        return Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<String> e) throws Exception {
                Call<AuthenticationResponse> callResponse = api.getAuthentication(authInfo);
                Response<AuthenticationResponse> response = callResponse.execute();

                if (response.isSuccessful()) {
                    String accessToken = response.body().getAccessToken();
                    e.onNext(accessToken);
                    e.onComplete();
                } else {
                    e.onError(new Throwable(response.message()));
                }
            }
        });
    }
}
